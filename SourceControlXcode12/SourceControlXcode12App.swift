//
//  SourceControlXcode12App.swift
//  SourceControlXcode12
//
//  Created by Alex Lima on 2021-04-25.
//

import SwiftUI

@main
struct SourceControlXcode12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
