//
//  ContentView.swift
//  SourceControlXcode12
//
//  Created by Alex Lima on 2021-04-25.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world! Xcode 12 Branch")
            .padding()
            .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
